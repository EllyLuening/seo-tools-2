# ~*~ encoding: utf-8 ~*~
import argparse

from app import db, models


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create new user')
    parser.add_argument('username', help='select username for user')
    parser.add_argument('password', help='select password for user')
    args = parser.parse_args()
    try:
        user = models.User(username=args.username, password=args.password)
        db.session.add(user)
        db.session.commit()
    except BaseException, e:
        print('Sorry, there was exception \n{}'.format(e.message))
    else:
        print('{} successfully added.'.format(user))

